<?php

require_once './api/Jeep.php';
$jeep = new Jeep();

//Write your code here
$jeep->move_forward(10)
        ->dump_fuel(5)
        ->move_back(10)
        ->collect_fuel(20)
        ->move_forward(10)
        ->collect_fuel(5)
        ->move_forward(10)
        ->run();

