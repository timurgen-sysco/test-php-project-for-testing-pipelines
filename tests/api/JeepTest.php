<?php

require_once './api/Jeep.php';

use PHPUnit\Framework\TestCase;

/**
 * Description of JeepTest
 *
 * @author 100tsa
 */
class JeepTest extends TestCase {

    protected $jeepInstanse;

    protected function setUp(): void {
        $this->jeepInstanse = new Jeep();
    }

    protected function tearDown(): void {
        
    }

    public function testSimple() {
        $this->expectException(InvalidArgumentException::class);
        $this->jeepInstanse->collect_fuel(-5);
    }

    public function testSimple2() {
        $this->assertObjectHasAttribute('commands', $this->jeepInstanse->collect_fuel(5));
    }

}
