<?php

/**
 * Provides required functions to build a sequence of commands
 * issued to remote frontend server to resolve "jeep problem"
 * Some simple limitations:
 *     • If dumps fuel on base station - fuel is wasted
 *     • If dumps fuel on already full depot - fuel is wasted
 *     • If dumps more fuel than have in tank - all fuel will be dumped
 *
 * @author Sysco Middleware programmers
 */
class Jeep {

    private $commands = [];
    private $is_running = false;

    /**
     * Move Jeep some distance forward
     * @param int $distance distance to move expressed as integer number
     * @return \Jeep returns itself for chaining
     * @throws InvalidArgumentException if distance is negative
     */
    public final function move_forward(int $distance): Jeep {
        if ($distance < 0) {
            throw new InvalidArgumentException("Distance must be positive or equal to 0");
        }

        $this->commands[] = ["move" => $distance];
        return $this;
    }

    /**
     * Move Jeep some distance back
     * @param int $distance distance to move expressed as integer number
     * @return \Jeep returns itself for chaining
     * @throws InvalidArgumentException if distance is negative
     */
    public final function move_back(int $distance): Jeep {
        if ($distance < 0) {
            throw new InvalidArgumentException("Distance must be positive or equal to 0");
        }
        $this->commands[] = ["move" => -$distance];
        return $this;
    }

    /**
     * Collects some fuel from base storage or stored fuel depot.
     *   If amount is bigger than available amount then only available amount will be collected.
     *   Base storage has infinite fuel
     * @param int $amount amount to collect
     * @return \Jeep eturns itself for chaining
     * @throws InvalidArgumentException if fuel amount is negative
     */
    public final function collect_fuel(int $amount): Jeep {
        if ($amount < 0) {
            throw new InvalidArgumentException("Fuel amount must be positive or equal to 0");
        }
        $this->commands[] = ["collect" => $amount];
        return $this;
    }

    /**
     * Dumps some fuel into a barrel placed at current point
     * If dumps fuel on already full depot - fuel is wasted
     * @param int $amount amount of fuel to dump
     * @return \Jeep returns itself fr chaining
     * @throws InvalidArgumentException if fuel amount is negative
     */
    public final function dump_fuel(int $amount): Jeep {
        if ($amount < 0) {
            throw new InvalidArgumentException("Fuel amount must be positive or equal to 0");
        }
        $this->commands[] = ["dump" => $amount];
        return $this;
    }

    /**
     * Function to run Jeep, can only be emitted once and must be last function in your program
     * @return void
     * @throws RuntimeException if anything goes wrong
     */
    public final function run(): void {
        if ($this->is_running) {
            throw new RuntimeException("Already running...");
        }

        $this->is_running = true;
        $string_to_send = json_encode(array_values(($this->commands)));
        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => $string_to_send
            ), "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents(getenv('APP_URL'), false, $context);
        if (!empty($result)) {
            throw new RuntimeException($result);
        }
    }

}
